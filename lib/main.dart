import 'package:flutter/material.dart';
import 'package:flutter_app_lock_ble/Pages/Login.dart';
import 'package:flutter_app_lock_ble/Pages/PageAdmin.dart';
import 'package:flutter_app_lock_ble/Pages/PageUser.dart';
import 'package:flutter_app_lock_ble/Pages/MyHomePage.dart';
import 'package:flutter_launcher_icons/android.dart';
import 'package:flutter_launcher_icons/constants.dart';
import 'package:flutter_launcher_icons/custom_exceptions.dart';
import 'package:flutter_launcher_icons/ios.dart';
import 'package:flutter_launcher_icons/main.dart';
import 'package:flutter_launcher_icons/utils.dart';
import 'package:flutter_launcher_icons/xml_templates.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Login(),
      routes: {
        '/PageAdmin': (context) => PageAdmin(),
        '/PageUser': (context) => PageUser(),
        '/MyHomePage': (context) => MyHomePage(),
      },
    );
  }
}