import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_app_lock_ble/Pages/MyHomePage.dart';
import 'package:flutter_app_lock_ble/Pages/Drawer.dart';

class PageAdmin extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final Send_Data_connect args = ModalRoute.of(context).settings.arguments;

    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
        backgroundColor: Colors.blue[50],
        appBar: AppBar(
          toolbarHeight: 75,
          elevation: 0.5,
          centerTitle: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(45),
              // top: Radius.circular(45)
            ),
          ),
          backgroundColor: Colors.red,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.segment),
                padding: EdgeInsets.only(left: 20.0),
                iconSize: 25,
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          title: Text(
            'DashBoard Device',
            style: TextStyle(
              fontFamily: 'Varela',
              fontSize: 20,
              foreground: Paint()..color = Colors.white,
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.arrow_forward),
              iconSize: 25,
              tooltip: 'Show Snackbar',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage()),
                );
              },
            ),
            SizedBox(
              width: 20.0,
            )
          ],
        ),
        drawer: InkWellDrawer(),
        body: ListView(
          children: [
            Container(

              // margin: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                padding: EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                margin: const EdgeInsets.only(
                    left: 16, right: 16, top: 15, bottom: 5),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.white, Colors.white],
                    ),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0),
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0))),
                child: Center(
                    child: ListTile(
                      // contentPadding: EdgeInsets.all(30.0),
                        leading: Container(
                          padding: EdgeInsets.only(right: 0),
                          child: Image(
                            image: AssetImage('images/icon_Bluetooth.png'),
                            fit: BoxFit.cover,
                            width: 50.0,
                          ),
                        ),
                        title: Text(
                          args.title,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(
                          args.id,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Colors.black45,
                              fontSize: 12.0,
                              fontWeight: FontWeight.normal),
                        ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              padding: EdgeInsets.all(5),
                              child: Icon(
                                Icons.signal_cellular_alt_outlined,
                                color: Colors.blue,
                                size: 30,
                              ),
                            ),
                          ],
                        )))),
            Container(
                padding: EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                margin: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 5),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.white, Colors.white],
                    ),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0),
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0))),
                child: Center(
                    child: ListTile(
                      // contentPadding: EdgeInsets.only(left: 10.0),
                      leading: Container(
                        child: Image(
                          image: AssetImage('images/box_icon.png'),
                          fit: BoxFit.cover,
                          width: 50.0,
                        ),
                      ),
                      title: Text(
                        'Welcome Admin',
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        'Please set  OTP to Box ',
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black45,
                            fontSize: 12.0,
                            fontWeight: FontWeight.normal),
                      ),
                    ))),
            Container(
              // margin: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
                padding: EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                margin: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 0),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.white, Colors.white],
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0),
                    )),
                child: Center(
                  child: ListTile(
                      title: Text(
                        'Set OTP',
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Icon(
                              Icons.arrow_circle_down_rounded,
                              color: Colors.black,
                              size: 40,
                            ),
                          ),
                        ],
                      )),
                )),
            const Divider(
              color: Colors.black,
              height: 5,
              thickness: 5,
              indent: 16,
              endIndent: 16,
            ),
            Container(
                padding: EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 5.0, bottom: 0.0),
                margin: const EdgeInsets.only(
                    left: 16, right: 16, top: 0, bottom: 5),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.white, Colors.white],
                    ),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0))),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 15,bottom: 5),
                      child: new TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(25.0),
                              ),
                            ),
                            labelText: 'Insert OTP',
                            contentPadding: EdgeInsets.all(15.0)),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(
                            left: 100, right: 100, top: 15, bottom: 15),
                        alignment: Alignment.center,
                        child: Row(
                          children: [
                            Expanded(
                                child: ButtonTheme(
                                    height: 50.0,
                                    child: RaisedButton(
                                      color: Colors.black,
                                      // onPressed: () {
                                      //   characteristic.write(
                                      //       "sssss");
                                      // },
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(30.0),
                                        // side: BorderSide(color: Colors.red)
                                      ),
                                      child: Text(
                                        "Send",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 22,
                                          fontFamily: 'Fira',
                                        ),
                                      ),
                                    )))
                          ],
                        ))
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
