import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:flutter_app_lock_ble/Pages/Drawer.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app_lock_ble/Pages/AppBarAdminUser.dart';
import 'package:flutter_app_lock_ble/Pages/AppBarDefault.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sweetalert/sweetalert.dart';

ProgressDialog progressDialog;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  SharedPreferences sharedPreferences;
  final String title;
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  final List<BluetoothDevice> devicesList = new List<BluetoothDevice>();
  final Map<Guid, List<int>> readValues = new Map<Guid, List<int>>();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class Send_Data_connect {
  final String title;
  final String id;

  Send_Data_connect(this.title, this.id);
}

class _MyHomePageState extends State<MyHomePage> {
  final _writeController_user = TextEditingController();
  final _writeController_admin = TextEditingController();
  String usertype = "";
  String Check_device = "";
  String Check_state_lock = "";
  String Checkstate_device = "";
  String Checkstate_device_admin = "ULOCK";
  String Checkstate_device_users = "LOCK";

  BluetoothDevice _connectedDevice;
  List<BluetoothService> _services;

  void get_Data_Login() async {
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('username') ?? '';
    usertype = myString;
    // print('$myString myString');
  }

  void get_Data_Device() async {
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('device_name') ?? '';
    Check_device = myString;
    // print('$myString myString');
  }

  void clearText() {
    _writeController_user.clear();
  }

  _addDeviceTolist(final BluetoothDevice device) {
    if (!widget.devicesList.contains(device)) {
      setState(() {
        widget.devicesList.add(device);
      });
    }
  }

  /*Scan for devices*/
  @override
  void initState() {
    super.initState();
    get_Data_Login();
    get_Data_Device();
    widget.flutterBlue.connectedDevices
        .asStream()
        .listen((List<BluetoothDevice> devices) {
      for (BluetoothDevice device in devices) {
        _addDeviceTolist(device);
      }
    });

    // Listen to scan results
    widget.flutterBlue.scanResults.listen((List<ScanResult> results) {
      // scan results
      for (ScanResult result in results) {
        if (result.device.name != '') {
          print("<------------------------------------------------>");
           var add_riss = [];
          Map<String, dynamic> map = {
            'rssi': result.rssi,
          };
          final toJson = json.encode(map);
          add_riss.add(toJson);
          add_riss.add(result.device);
          print(add_riss);
          print("<------------------------------------------------>");

          _addDeviceTolist(result.device);

        }
      }
    });

    // Start scanning
    widget.flutterBlue.startScan(timeout: Duration(seconds: 4));
  }

  ListView _buildListViewOfDevices() {
    List<Container> containers = new List<Container>();
    for (BluetoothDevice device in widget.devicesList) {
      //loop ui list device
      containers.add(
        Container(
          height: 90,
          margin: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.white, Colors.white, Colors.white],
              ),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                  bottomLeft: Radius.circular(25.0),
                  bottomRight: Radius.circular(25.0))),
          child: Center(
            child: ListTile(
                leading: Container(
                  padding: EdgeInsets.only(right: 0),
                  child: Image(
                    image: AssetImage('images/icon_Bluetooth.png'),
                    fit: BoxFit.cover,
                    width: 45.0,
                  ),
                ),
                title: Text(
                  // device.name == '' ? 'N/A' : device.name,
                  device.name,
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  device.id.toString(),
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black45,
                      fontSize: 10.0,
                      fontWeight: FontWeight.normal),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [

                    Container(
                      padding: EdgeInsets.all(5),
                      child:
                      Icon(
                        Icons.signal_cellular_alt_rounded,
                        size: 35,
                        color: Colors.blue,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: IconButton(
                        icon: Icon(Icons.swap_vertical_circle, size: 35),
                        color: Colors.pinkAccent,
                        onPressed: () async {
                          widget.flutterBlue.stopScan();
                          progressDialog.show();
                          try {
                            // Connect to the device
                            await device.connect();
                          } catch (e) {
                            if (e.code != 'already_connected') {
                              throw e;
                            }
                          } finally {
                            // Discover services
                            _services = await device.discoverServices();
                          }
                          Check_device = device.name.toString();
                          SharedPreferences preferences =
                              await SharedPreferences.getInstance();
                          preferences.setString(
                              'device_name', device.name.toString());

                          Future.delayed(Duration(seconds: 2)).then((value) {
                            progressDialog.hide().whenComplete(() {
                              setState(() {
                                _connectedDevice = device;
                              });
                            });
                          });
                        },
                      ),
                    ),
                  ],
                )),
          ),
        ),
      );
    }

    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }
  ListView _buildConnectDeviceView() {
    for (BluetoothService service in _services) {
      for (BluetoothCharacteristic characteristic in service.characteristics) {
        if (characteristic.uuid.toString() ==
            'ff893610-976b-4ad3-9993-d1338d1257fd') {
          print("1====>");
          // _getdata_ReadValue(characteristic);
          print("2====>");
          // _getdata_ReadValue(characteristic);
        }
        if (characteristic.uuid.toString() ==
                '972731f7-4ed7-4677-b265-e8c552185aa0' &&
            usertype == "admin") {
          return _buildView_Admin(characteristic);
        } else if (characteristic.uuid.toString() ==
            'da467c88-9e6a-4d73-8f15-1dca6bc67419') {
          return _buildView_User(characteristic);
        }
      }
    }
  }

  _getdata_ReadValue(BluetoothCharacteristic characteristic) async {
    // var cha = "$(ff893610-976b-4ad3-9993-d1338d1257fd)";
    print("_getdata_ReadValue-------->");
    var sub = characteristic.value.listen((value) {
      widget.readValues[characteristic.uuid] = value;
      String data = utf8.decode(value);
      Checkstate_device = data;
    });
    await characteristic.read();
    sub.cancel();
  }

  ListView _buildView_Admin(BluetoothCharacteristic characteristic) {
    List<Container> containers = new List<Container>();
    containers.add(
      Container(
        // margin: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 15, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
                bottomLeft: Radius.circular(25.0),
                bottomRight: Radius.circular(25.0))),
        child: Center(
            child: ListTile(
                // contentPadding: EdgeInsets.all(30.0),
                leading: Container(
                  padding: EdgeInsets.only(right: 0),
                  child: Image(
                    image: AssetImage('images/icon_Bluetooth.png'),
                    fit: BoxFit.cover,
                    width: 50.0,
                  ),
                ),
                title: Text(
                  Check_device,
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  characteristic.deviceId.toString(),
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black45,
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.signal_cellular_alt_outlined,
                        color: Colors.blue,
                        size: 30,
                      ),
                    ),
                  ],
                )
            )),
      ),
    );
    containers.add(Container(
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
                bottomLeft: Radius.circular(25.0),
                bottomRight: Radius.circular(25.0))),
        child: Center(
            child: ListTile(
          // contentPadding: EdgeInsets.only(left: 10.0),
          leading: Container(
            child: Image(
              image: AssetImage('images/box_icon.png'),
              fit: BoxFit.cover,
              width: 50.0,
            ),
          ),
          title: Text(
            'Welcome Admin',
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.black,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            'Please set  OTP to Box ',
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.black45,
                fontSize: 12.0,
                fontWeight: FontWeight.normal),
          ),
              trailing: Checkstate_device_admin == 'LOCK'
                  ? Icon(
                Icons.lock,
                size: 35,
                color: Colors.red,
              )
                  : Icon(
                Icons.lock_open,
                size: 35,
                color: Colors.green,
              ),
        ))));
    containers.add(Container(
        // margin: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 0.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 0),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0),
            )),
        child: Center(
          child: ListTile(
              title: Text(
                'Set OTP',
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.arrow_circle_down_rounded,
                      color: Colors.black,
                      size: 40,
                    ),
                  ),
                ],
              )),
        )));
    containers.add(Container(
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 0.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25.0),
                bottomRight: Radius.circular(25.0))),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
              child: new TextField(
                maxLength: 6,
                keyboardType: TextInputType.number,
                controller: _writeController_admin,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(25.0),
                      ),
                    ),
                    labelText: 'Insert OTP',
                    contentPadding: EdgeInsets.all(15.0)),
              ),
            ),
            Container(
                margin:
                    EdgeInsets.only(left: 100, right: 100, top: 10, bottom: 15),
                alignment: Alignment.center,
                child: Row(
                  children: [
                    Expanded(
                        child: ButtonTheme(
                            height: 45.0,
                            child: RaisedButton(
                              color: Colors.black,
                              onPressed: () async {
                                progressDialog.show();
                                final admin = "12345676";

                                if (_writeController_admin.value.text != "") {



                                  String send_data =
                                      admin + _writeController_admin.value.text;
                                  characteristic.write(utf8.encode(send_data));

                                  Future.delayed(Duration(seconds: 2))
                                      .then((value) {
                                    progressDialog.hide().whenComplete(() {
                                      SweetAlert.show(context,
                                          title: "Send OTP success",
                                          subtitle: "success",
                                          style: SweetAlertStyle.success);
                                      setState(() {
                                        Checkstate_device_admin = "LOCK";
                                      });
                                      clearText();
                                    });
                                  });
                                  // _getdata_ReadValue(characteristic);
                                } else {
                                  Future.delayed(Duration(seconds: 1))
                                      .then((value) {
                                    progressDialog.hide().whenComplete(() {
                                      SweetAlert.show(context,
                                          title: "Unlock fail",
                                          subtitle: "OTP incorrect",
                                          style: SweetAlertStyle.error);

                                      clearText();
                                    });
                                  });
                                }
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              child: Text(
                                "Send",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontFamily: 'Fira',
                                ),
                              ),
                            )))
                  ],
                ))
          ],
        )));
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }

  ListView _buildView_User(BluetoothCharacteristic characteristic) {
    List<Container> containers = new List<Container>();
    containers.add(
      Container(
        // margin: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 15, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
                bottomLeft: Radius.circular(25.0),
                bottomRight: Radius.circular(25.0))),
        child: Center(
            child: ListTile(
                // contentPadding: EdgeInsets.all(30.0),
                leading: Container(
                  padding: EdgeInsets.only(right: 0),
                  child: Image(
                    image: AssetImage('images/icon_Bluetooth.png'),
                    fit: BoxFit.cover,
                    width: 50.0,
                  ),
                ),
                title: Text(
                  Check_device,
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  characteristic.deviceId.toString(),
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black45,
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.signal_cellular_alt_outlined,
                        color: Colors.blue,
                        size: 30,
                      ),
                    ),
                  ],
                ))),
      ),
    );
    containers.add(Container(
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
                bottomLeft: Radius.circular(25.0),
                bottomRight: Radius.circular(25.0))),
        child: Center(
            child: ListTile(
          // contentPadding: EdgeInsets.only(left: 10.0),
          leading: Container(
            child: Image(
              image: AssetImage('images/logo_key.png'),
              fit: BoxFit.cover,
              width: 50.0,
            ),
          ),
          title: Text(
            'Welcome User',
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.black,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            'Check State Lock or Unlock',
            style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.black45,
                fontSize: 12.0,
                fontWeight: FontWeight.normal),
          ),
          trailing: Checkstate_device_users == 'LOCK'
              ? Icon(
            Icons.lock,
            size: 35,
            color: Colors.red,
                )
              : Icon(


            Icons.lock_open,
            size: 35,
            color: Colors.green,
                ),
        ))));
    containers.add(Container(
        // margin: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 0.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 0),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0),
            )),
        child: Center(
          child: ListTile(
              title: Text(
                'OTP',
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.arrow_circle_down_rounded,
                      color: Colors.black,
                      size: 40,
                    ),
                  ),
                ],
              )),
        )));
    containers.add(Container(
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 0.0),
        margin: const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white],
            ),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25.0),
                bottomRight: Radius.circular(25.0))),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
              child: new TextField(
                maxLength: 6,
                keyboardType: TextInputType.number,
                controller: _writeController_user,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(25.0),
                      ),
                    ),
                    labelText: 'Insert OTP',
                    contentPadding: EdgeInsets.all(15.0)),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(18, 12, 18, 25),
              child: Row(
                children: [
                  Expanded(
                      child: ButtonTheme(
                          height: 50.0,
                          child: RaisedButton(
                            color: Colors.blue,
                            onPressed: () {
                              progressDialog.show();
                              if (_writeController_user.value.text != "") {


                                characteristic.write(utf8
                                    .encode(_writeController_user.value.text));
                                Checkstate_device = "";
                                Future.delayed(Duration(seconds: 2))
                                    .then((value) {
                                  progressDialog.hide().whenComplete(() {
                                    SweetAlert.show(context,
                                        title: "Unlock success",
                                        subtitle: "success",
                                        style: SweetAlertStyle.success);

                                    setState(() {
                                      Checkstate_device_users = "ULOCK";
                                    });
                                    clearText();
                                  });
                                });
                              } else {
                                Future.delayed(Duration(seconds: 1))
                                    .then((value) {
                                  progressDialog.hide().whenComplete(() {
                                    SweetAlert.show(context,
                                        title: "Unlock fail",
                                        subtitle: "OTP incorrect",
                                        style: SweetAlertStyle.error);
                                    clearText();
                                  });
                                });
                              }
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              // side: BorderSide(color: Colors.red)
                            ),
                            child: Text(
                              "SEND OTP",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontFamily: 'Varela',
                              ),
                            ),
                          )))
                ],
              ),
            ),
          ],
        )));
    // containers.add(
    //     Container(
    //         padding: EdgeInsets.fromLTRB(75, 15, 75, 0),
    //         child: SizedBox.fromSize(
    //           size: Size(80, 120), // button width and height
    //           child: ClipOval(
    //             child: Material(
    //               color: Colors.red, // button color
    //               child: InkWell(
    //                 splashColor: Colors.white, // splash color
    //                 onTap: () {}, // button pressed
    //                 child: Column(
    //                   mainAxisAlignment: MainAxisAlignment.center,
    //                   children: <Widget>[
    //                     Icon(Icons.lock, color: Colors.white), // icon
    //                     Text(
    //                       "LOCK",
    //                       style: TextStyle(
    //                           color: Colors.white,
    //                           fontSize: 18,
    //                           fontFamily: 'Varela'),
    //                     ), // text
    //                   ],
    //                 ),
    //               ),
    //             ),
    //           ),
    //         ))
    //
    // );
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }

  ListView _buildView() {
    if (_connectedDevice != null) {
      return _buildConnectDeviceView();
    }
    return _buildListViewOfDevices();
  }

  _buildViewAppBar() {
    if (_connectedDevice != null) {
      return AppBarAdminUser();
    }
    return AppBarDefault();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(
      message: 'Please wait...',
    );

    return Scaffold(
      backgroundColor: Colors.blue[50],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0), // here the desired height
          child: _buildViewAppBar()),
      drawer: InkWellDrawer(),
      body: _buildView(),
    );
  }
}
