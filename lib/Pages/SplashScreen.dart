import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreenOne extends StatefulWidget {
  @override
  _SplashScreenOneState createState() => _SplashScreenOneState();
}

class _SplashScreenOneState extends State<SplashScreenOne> {

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }
  void navigationPage() {
    Navigator.of(context).pushReplacementNamed('/MyHomePage');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: new Color(0xff622F74),
                gradient: LinearGradient(
                  colors: [new Color(0xff3ec7fd), new Color(0xff29dfb7)],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                )),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(85, 0, 85, 0),
            child: Center(
              child: Image.asset('images/inetlogo.png'),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.yellow),
              ),
              Padding(padding: EdgeInsets.only(bottom: 80),  ),
            ],),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 25),
                child: Text('Created by @Giant Team',
                  style: TextStyle(color: Colors.white),

                ),
              ),
            ],)
        ],
      ),
    );
  }
}
