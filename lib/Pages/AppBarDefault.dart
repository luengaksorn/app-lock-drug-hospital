import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock_ble/Pages/MyHomePage.dart';
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog pr;

class AppBarDefault extends StatefulWidget {
  @override
  _AppBarDefaultPageState createState() => _AppBarDefaultPageState();
}

class _AppBarDefaultPageState extends State<AppBarDefault> {
  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      message: 'Please wait ...',
    );

    return new AppBar(
      toolbarHeight: 80,
      elevation: 0.5,
      centerTitle: true,
      backgroundColor: Colors.green,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(45)),
      ),
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(Icons.segment),
            padding: EdgeInsets.only(left: 20.0),
            iconSize: 30,
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          );
        },
      ),
      title: Text(
        'One Access App',
        style: TextStyle(
          fontFamily: 'Varela',
          fontSize: 22,
          foreground: Paint()..color = Colors.white,
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.replay_circle_filled),
          iconSize: 30,
          tooltip: 'Show Snackbar',
          onPressed: () {
            pr.show();
            Future.delayed(Duration(seconds: 2)).then((value) {
              pr.hide().whenComplete(() {
                Navigator.of(context).push(CupertinoPageRoute(
                    builder: (BuildContext context) => MyHomePage()));
              });
            });
          },
        ),
        SizedBox(
          width: 20.0,
        )
      ],
    );
  }
}
