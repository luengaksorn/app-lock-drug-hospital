import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock_ble/Pages/MyHomePage.dart';
import 'package:flutter_app_lock_ble/Pages/Drawer.dart';

class PageUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Scaffold(
            backgroundColor: Colors.blue[50],
            appBar: AppBar(
              toolbarHeight: 80,
              elevation: 0.5,
              centerTitle: true,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(45),
                ),
              ),
              backgroundColor: Colors.red,
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(Icons.segment),
                    padding: EdgeInsets.only(left: 20.0),
                    iconSize: 35,
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                    tooltip:
                    MaterialLocalizations.of(context).openAppDrawerTooltip,
                  );
                },
              ),
              title: Text(
                'Device 1',
                style: TextStyle(
                  fontFamily: 'Varela',
                  fontSize: 35,
                  foreground: Paint()..color = Colors.white,
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: const Icon(Icons.arrow_forward),
                  iconSize: 35,
                  tooltip: 'Show Snackbar',
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyHomePage()),
                    );
                  },
                ),
                SizedBox(
                  width: 20.0,
                )
              ],
            ),
            drawer: InkWellDrawer(),
            body: ListView(
              children: [
                Container(
                    padding: EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 20.0, bottom: 10.0),
                    margin: const EdgeInsets.only(
                        left: 12, right: 12, top: 20, bottom: 5),
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [Colors.white, Colors.white, Colors.white],
                        ),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25.0),
                            topRight: Radius.circular(25.0),
                            bottomLeft: Radius.circular(25.0),
                            bottomRight: Radius.circular(25.0))),
                    child: Center(
                        child: ListTile(
                          contentPadding: EdgeInsets.all(10.0),
                          leading: Container(
                            padding: EdgeInsets.only(right: 0),
                            child: Image(
                              image: AssetImage('images/icon_Bluetooth.png'),
                              fit: BoxFit.cover,
                              width: 70.0,
                            ),
                          ),
                          title: Text(
                            'Connecting : Device 1',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Colors.black,
                                fontSize: 25.0,
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(
                            '0x03',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Colors.black45,
                                fontSize: 20.0,
                                fontWeight: FontWeight.normal),
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                padding: EdgeInsets.all(5),
                                child: Icon(
                                  Icons.signal_cellular_alt_outlined,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                              ),
                            ],
                          ),
                        ))),
                Container(
                  margin: const EdgeInsets.only(
                      left: 12, right: 12, top: 10, bottom: 0),
                  padding:
                  EdgeInsets.only(left: 5.0, right: 5, top: 0, bottom: 0),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Colors.white, Colors.white, Colors.white],
                      ),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0),
                        // bottomLeft: Radius.circular(25.0),
                        // bottomRight: Radius.circular(25.0)
                      )),
                  child: Center(
                    child: ListTile(
                      contentPadding: EdgeInsets.fromLTRB(15, 25, 15, 25),
                      leading: Container(
                        child: Image(
                          image: AssetImage('images/logo_key.png'),
                          fit: BoxFit.cover,
                          width: 55.0,
                          height: 55,
                        ),
                      ),
                      title: Text(
                        'Welcome User',
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        'Check State Lock or Unlock',
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black45,
                            fontSize: 18.0,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                ),

                Container(
                  margin: const EdgeInsets.only(
                      left: 12, right: 12, top: 0, bottom: 0),
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20, top: 0, bottom: 20),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Colors.white, Colors.white, Colors.white],
                      ),
                      borderRadius: BorderRadius.only(
                        // topLeft: Radius.circular(25.0),
                        // topRight: Radius.circular(25.0),
                          bottomLeft: Radius.circular(25.0),
                          bottomRight: Radius.circular(25.0))),
                  // padding: EdgeInsets.only(left: 20, right: 20, top: 30),
                  child: new TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(25.0),
                          ),
                        ),
                        labelText: 'OTP',
                        contentPadding: EdgeInsets.all(20.0)),
                  ),
                ),
                // Container(
                //     margin: EdgeInsets.only(
                //         left: 100, right: 100, top: 30, bottom: 20),
                //     alignment: Alignment.center,
                //     child: Row(
                //       children: [
                //         Expanded(
                //             child: ButtonTheme(
                //                 height: 50.0,
                //                 child: RaisedButton(
                //                   color: Colors.black,
                //                   onPressed: () {
                //                     Navigator.push(
                //                       context,
                //                       MaterialPageRoute(
                //                           builder: (context) => HomePage()),
                //                     );
                //                   },
                //                   shape: RoundedRectangleBorder(
                //                     borderRadius: BorderRadius.circular(30.0),
                //                     // side: BorderSide(color: Colors.red)
                //                   ),
                //                   child: Text(
                //                     "Submit",
                //                     style: TextStyle(
                //                       color: Colors.white,
                //                       fontSize: 22,
                //                       fontFamily: 'Fira',
                //                     ),
                //                   ),
                //                 )))
                //       ],
                //
                Container(
                  padding: EdgeInsets.fromLTRB(12, 15, 12, 0),
                  child: Row(
                    children: [
                      Expanded(
                          child: ButtonTheme(
                              height: 50.0,
                              child: RaisedButton(
                                color: Colors.blue,
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyHomePage()),
                                  );
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  // side: BorderSide(color: Colors.red)
                                ),
                                child: Text(
                                  "SEND OTP",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontFamily: 'Varela',
                                  ),
                                ),
                              )))
                    ],
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(75, 15, 75, 0),
                    child: SizedBox.fromSize(
                      size: Size(200, 200), // button width and height
                      child: ClipOval(
                        child: Material(
                          color: Colors.red, // button color
                          child: InkWell(
                            splashColor: Colors.white, // splash color
                            onTap: () {}, // button pressed
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.lock, color: Colors.white), // icon
                                Text(
                                  "LOCK",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontFamily: 'Varela'),
                                ), // text
                              ],
                            ),
                          ),
                        ),
                      ),
                    ))
              ],
            )));
  }
}
