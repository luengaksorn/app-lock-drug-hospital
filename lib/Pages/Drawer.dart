import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock_ble/Pages/Login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InkWellDrawer extends StatefulWidget {
  InkWellDrawer({Key key, this.title}) : super(key: key);
  SharedPreferences sharedPreferences;
  final String title;

  @override
  _InkWellDrawerState createState() => _InkWellDrawerState();
}

class _InkWellDrawerState extends State<InkWellDrawer> {
  String usertype = "";

  @override
  void initState() {
    get_Data_Login();
  }

  @override
  void get_Data_Login() async {
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('username') ?? '';
    usertype = myString;
    print('$myString myString');
  }

  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: <Color>[
                Colors.green,
                Colors.greenAccent,
                Colors.green[200]
              ])),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Material(
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      elevation: 5,
                      child: Image.asset("images/icon_avatar.png",
                          height: 90, width: 90),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 15, right: 5),
                          child: Text(
                            'Welcome',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20.0),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 18),
                            child: Text(
                              usertype,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 15.0),
                            )),
                      ],
                    ),
                  ],
                ),
              )),
          Container(
              margin: EdgeInsets.only(right: 40, left: 40, top: 390),
              alignment: Alignment.bottomCenter,
              // padding: EdgeInsets.fromLTRB(60, 20, 60, 0),
              // height: 60,
              child: Row(
                children: [
                  Expanded(
                      child: ButtonTheme(
                          height: 50.0,
                          child: RaisedButton(
                            color: Colors.red,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Login()),
                              );
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              // side: BorderSide(color: Colors.red)
                            ),
                            child: Text(
                              "LOGOUT",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontFamily: 'Varela',
                              ),
                            ),
                          )))
                ],
              ))
        ],
      ),
    );
  }
}
