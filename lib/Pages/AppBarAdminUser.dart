import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock_ble/Pages/MyHomePage.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AppBarAdminUser extends StatefulWidget {
  AppBarAdminUser({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AppBarAdminUserState createState() => _AppBarAdminUserState();
}
class _AppBarAdminUserState extends State<AppBarAdminUser> {
  String usertype = "";
  ProgressDialog progressDialog;
  var percentage = 0;

  @override
  void initState() {
    get_Data_Login();
  }
  void get_Data_Login() async {
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('username') ?? '';
    usertype = myString;
    print('$myString myString');
  }

  @override
  Widget build(BuildContext context) {
    return new AppBar(
        toolbarHeight: 80,
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.green,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(45)),
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.segment),
              padding: EdgeInsets.only(left: 20.0),
              iconSize: 30,
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        title: Text(
          'DashBoard Device',
          style: TextStyle(
            fontFamily: 'Varela',
            fontSize: 22,
            foreground: Paint()..color = Colors.white,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.arrow_forward_sharp),
            iconSize: 30,
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyHomePage()),
              );
            },
          ),
          SizedBox(
            width: 20.0,
          )
        ],
    );
  }
}
