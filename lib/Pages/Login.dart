import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_lock_ble/Pages/MyHomePage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app_lock_ble/Pages/SplashScreen.dart';

TextEditingController usernameController = TextEditingController();
TextEditingController passwordController = TextEditingController();

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class ImageAssetState extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 15, 15, 30),
      child: Center(
        child: Image.asset('images/inetlogo.png'),
      ),
    );
  }
}

Widget buildUsername() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,

        decoration: BoxDecoration(
            color: Colors.white,

            borderRadius: BorderRadius.circular(25),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
            ]),

        child: TextField(
          style: TextStyle(color: Colors.black87),
          controller: usernameController,

          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),

              prefixIcon: Icon(
                Icons.account_circle_rounded,
                color: Color(0xFF474747),
              ),
              hintText: 'Username',
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}

Widget buildPassword() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(25),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
            ]),

        child: TextField(

          controller: passwordController,
          obscureText: true,
          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(

              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.lock,
                color: Color(0xFF474747),
              ),
              hintText: 'Password',
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}

class _LoginState extends State<Login> {

  void dispose(){
    usernameController.clear();
    super.dispose();
  }

  Future checkLogin() async {

    String username = usernameController.text;
    String password = passwordController.text;
    if (usernameController.text == username &&
        passwordController.text == password &&
        usernameController.text != "" &&
        passwordController.text != "") {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.setString('username', usernameController.text);
      // print(usernameController.text);
      // print(passwordController.text);
      if(passwordController.text == "123456"){
        print("dasfds");
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (context) => Page2Admin(),
        //     )
        // );
      }else{
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SplashScreenOne(),
          ),
        );
        Fluttertoast.showToast(
            msg: "Login Success",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.TOP,
            timeInSecForIos: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16);
      }}
    else {
      Fluttertoast.showToast(
          msg: "Username & Password Invalid!!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16);
    }
  }
  @override
  Widget build(BuildContext context) {
    void clearText() {
      usernameController.clear();
      passwordController.clear();
    }

    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
                child: Stack(children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
                      height: double.infinity,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.blueGrey[50],
                                Colors.blueGrey[100],
                              ])),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ImageAssetState(),
                            buildUsername(),
                            buildPassword(),
                            Container(
                                margin: EdgeInsets.only(top: 20),
                                alignment: Alignment.center,
                                padding: EdgeInsets.fromLTRB(0, 7, 0, 0),
                                // height: 60,
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: ButtonTheme(
                                            height: 50.0,
                                            child: RaisedButton(
                                              color: Colors.green,
                                              onPressed: () {
                                                checkLogin();
                                                // clearText();
                                              },
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.circular(30.0),
                                                // side: BorderSide(color: Colors.red)
                                              ),
                                              child: Text(
                                                "LOGIN",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ),
                                            )))
                                  ],
                                ))
                          ]))
                ]))));
  }
}